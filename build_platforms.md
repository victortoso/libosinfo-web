# Supported build platforms

libosinfo aims to support building and executing on multiple host OS platforms.
This appendix outlines which platforms are the major build targets. This
document outlines which platforms are targeted for each of these areas.
These platforms are used as the basis for deciding upon the
minimum required versions of 3rd party software libosinfo depends on. The
supported platforms are the targets for automated testing performed by
the project when patches are submitted for review, and tested before and
after merge.

If a platform is not listed here, it does not imply that libosinfo or its
related tools won't work. If an unlisted platform has comparable software
versions to a listed platform, there is every expectation that it will work.
Bug reports are welcome for problems encountered on unlisted platforms unless
they are clearly older vintage than what is described here.

Note that when considering software versions shipped in distros as
support targets, libosinfo considers only the version number, and assumes the
features in that distro match the upstream release with the same
version. In other words, if a distro backports extra features to the
software in their distro, libosinfo upstream code will not add explicit
support for those backports, unless the feature is auto-detectable in a
manner that works for the upstream releases too.

The [Repology](https://repology.org/) site is a useful resource to identify
currently shipped versions of software in various operating systems,
though it does not cover all distros listed below.


### Linux, FreeBSD and macOS

The project aims to support the most recent major version at all times. Support
for the previous major version will be dropped 2 years after the new major
version is released or when the vendor itself drops support, whichever comes
first. In this context, third-party efforts to extend the lifetime of a distro
are not considered, even when they are endorsed by the vendor (e.g. Debian
LTS); the same is true of repositories that contain packages backported from
later releases (e.g. Debian backports). Within each major release, only the
most recent minor release is considered.

For the purposes of identifying supported software versions available on Linux,
the project will look at CentOS, Debian, Fedora, openSUSE, RHEL, SLES and
Ubuntu LTS. Other distros will be assumed to ship similar software versions.

For FreeBSD, decisions will be made based on the contents of the ports tree;
for macOS, [Homebrew](https://brew.sh/) will be used, although
[MacPorts](https://www.macports.org/) is expected to carry similar versions.

### Windows

The project supports building with current versions of the MinGW toolchain,
hosted on Linux.

The version of the Windows API that's currently targeted is Vista / Server
2008.
