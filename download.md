## Table of contents

- [Current releases](#releases)
- [Deployment procedure](#deployment)
- [Signing keys](#signing)

<a name="releases"></a>
## Current releases

### Osinfo database

The Osinfo database is updated frequently (whenever there is a relevant set
of new content) and so individual releases are not listed here. Please consult
the [download site](https://releases.pagure.org/libosinfo/) directly for
release archives, named with the filename pattern ``osinfodb-$DATE.tar.xz``.

### Osinfo database tools

#### [osinfo-db-tools 1.10.0](https://releases.pagure.org/libosinfo/osinfo-db-tools-1.10.0.tar.xz) [(gpg)](https://releases.pagure.org/libosinfo/osinfo-db-tools-1.10.0.tar.xz.asc) Feb 14th, 2022

Changes in this release include

* Port to libsoup3 (optional)
* Several CI improvements
* Several translation improvements

#### [libosinfo 1.10.0](https://releases.pagure.org/libosinfo/libosinfo-1.10.0.tar.xz) [(gpg)](https://releases.pagure.org/libosinfo/libosinfo-1.10.0.tar.xz.asc) Feb 14th, 2022

Changes in this release include:

* Add API for resolving multiple tree matches
* Add API for resolving multiple media matches
* Add API to match between two OsinfoTree
* Add API to match between two OsinfoMedia
* Add API to get a complete list of firmwares
* Add missing documentation of osinfo_os_add_firmware()
* Add release status to osinfo-query
* Add --all flag to all tools to report all matches
* Fix hiding database entries
* Adapt to libsoup3 which is now preferred over libsoup2
* Several CI improvements
* Several translations improvements

All source archives for the previous releases are available for download at
the [release hosting site](https://releases.pagure.org/libosinfo/)

Previous release notes are available at in the
[NEWS file](https://gitlab.com/libosinfo/libosinfo/blob/master/NEWS).

<a name="deployment"></a>
### Deployment procedure

#### osinfo-db-tools

The osinfo-db-tools package is a pre-requisite of osinfo-db and libosinfo,
so should be built and installed first. It provides a small set of tools
for managing the installation, bundling and distribution of database files

```
$ tar zxvf osinfo-db-tools-$VERSION.tar.gz
$ cd osinfo-db-tools-$VERSION
$ meson setup build -Dprefix=/usr -Dsysconfdir=/etc -Dlocalstatedir=/var
$ ninja -C build
$ sudo ninja -C build install
```

#### osinfo-db

The osinfo-db package does not contain any code, nor does it require building,
as it is purely XML database files. Its contents merely have to be unpacked in
the correct location, using the osinfo-db-import tool. Operating system vendors
packaging it, should use the --system argument to install in the primary
database location.

```
$ sudo osinfo-db-import --system osinfo-db-$VERSION.tar.xz
```

Local system administrators wishing to provide updated content for a host
without conflicting with the OS vendor supplied packages should use the
``--local`` argument to install it in ``/etc``

```
$ sudo osinfo-db-import --local osinfo-db-$VERSION.tar.xz
```

Unprivileged users wishing to provide their account with updated content
should use the ``--user`` argument to install it

```
$ osinfo-db-import --user osinfo-db-$VERSION.tar.xz
```

#### libosinfo

The libosinfo package provides the C library for querying the database, with
ability to use it from non-C languages via GObject Introspection

```
$ tar zxvf libosinfo-$VERSION.tar.gz
$ cd libosinfo-$VERSION
$ meson setup build -Dprefix=/usr -Dsysconfdir=/etc -Dlocalstatedir=/var
$ ninja -C build
$ sudo ninja -C build install
```

<a name="signing"></a>
### Signing keys

Source tarballs for libosinfo formal release deliverable are signed with a
GPG signature. You should verify the signature before using the releases.

Releases are currently signed by [Victor Toso](static/keys/toso.asc), whose
key has the following fingerprint:

```
pub   rsa4096/0x97D9123DE37A484F 2016-02-09 [SC] [expires: 2024-12-04]
      Key fingerprint = 206D 3B35 2F56 6F3B 0E65  72E9 97D9 123D E37A 484F
uid                             Victor Toso de Carvalho <me@victortoso.com>
uid                             Victor Toso <toso@posteo.net>
uid                             Victor Toso de Carvalho (toso) <victortoso@redhat.com>
uid                             Victor Toso (toso) <victortoso@gnome.org>
uid                             Victor Toso de Carvalho <vtosodec@redhat.com>
```

Prior to October 2021, releases were signed by [Fabiano Fidêncio](static/keys/fidencio.asc),
whose key has the following fingerprint:

```
pub   rsa4096/0xEE926C2BDACC177B 2017-01-06 [SC]
      Key fingerprint = 09B9 C8FF 223E F113 AFA0  6A39 EE92 6C2B DACC 177B
uid                             Fabiano Fidêncio <fabiano@fidencio.org>
```

Prior to March 2019, releases were signed by [Daniel Berrangé](static/keys/berrange.asc),
whose key has the following fingerprint:

```
pub   rsa4096/0xBE86EBB415104FDF 2011-10-11 [SC]
      Key fingerprint = DAF3 A6FD B26B 6291 2D0E  8E3F BE86 EBB4 1510 4FDF
uid                             Daniel P. Berrange <berrange@redhat.com>
uid                             Daniel P. Berrange <dan@berrange.com>
```

Note that nightly releases of `osinfo-db` are not currently signed, as they are
produced with a fully automated system running in GitLab CI, using the current
contents of the Git repository.
